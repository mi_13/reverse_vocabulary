var express = require('express');
var router = express.Router();
var mapAPI = require('./mapAPI.js');

router.use('/textMap', mapAPI);

module.exports = router;
