var express = require('express');
var router = express.Router();
var _ = require('lodash');
var async = require('async');
var Morphy = require('phpmorphy').default;
var morphy = new Morphy('ru', {
//  nojo:                false,
  storage:             Morphy.STORAGE_MEM,
  predict_by_suffix:   true,
  predict_by_db:       true,
  graminfo_as_text:    true,
  use_ancodes_cache:   false,
  resolve_ancodes:     Morphy.RESOLVE_ANCODES_AS_TEXT
});

router.post('/generate', function (req, res) {
  var text = req.body.text;
  var textBaseMap = [];
  var textMap = [];

  async.waterfall([
      prepareText,
      checkMorphy,
      reverseWords
  ], function (err, map) {
    if (err) { return res.send({ err: err }); }

    return res.json({ map: map })
  });

  function prepareText(cb) {
    var wordsBaseMap = _(text).split(' ');

    cb(null, wordsBaseMap);
  }

  function checkMorphy(text, cb) {
    async.each(text, function (word, next) {
      var wordBaseForm = morphy.lemmatize(word);
      if (!wordBaseForm) { return next(); }

      textBaseMap.push(wordBaseForm);
      textBaseMap = _.uniq(textBaseMap);

      next();
    }, function (err) {
      if (err) { return cb(err); }

      cb()
    });
  }

  function reverseWords(cb) {
    var wordsCount = _(textBaseMap).countBy().value();
    for (var word in wordsCount) {
      textMap.push({name: word, count: wordsCount[word], reverse: word.split("").reverse().join("") });
    }

    cb(null, textMap);
  }
});

module.exports = router;
