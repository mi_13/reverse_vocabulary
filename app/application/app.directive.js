(function () {
  'use strict';

  angular
      .module('reverse_vocabulary')
      .directive('application', application);

  application.$inject = [];

  function application() {
    return {
      replace: true,
      controller: 'ApplicationController',
      controllerAs: 'ApplicationCtrl',
      templateUrl: 'application/application.html'
    };
  }
})();