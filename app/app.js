(function () {
  'use strict';

  angular
      .module('reverse_vocabulary', [
          'ngMaterial',
          'ngMessages',
          'chart.js'
      ]);
})();
